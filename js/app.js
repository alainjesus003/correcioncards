/*objeto en javascript*/
automovil={
    marca:"OTRA",
    modelo:"Yaris",
    descripcion:"Toyota Yaris 2024 Automatico Aire Acondicionado Precio $330,000",
    imagen:"/img/auto.jpg"
 }

 //mostrar objeto
 console.log("automovil" + automovil);
 console.log("marca: "+ automovil.marca);
 console.log("modelo: "+ automovil.modelo);
 console.log("descripcion: "+ automovil.descripcion);
 console.log("imagen: "+ automovil.imagen);

 const modelo = document.getElementById("modelo");
 modelo.innerHTML=automovil.modelo;

 const marca = document.getElementById("marca");
 marca.innerHTML=automovil.marca

 const descripcion = document.getElementById("descripcion");
 descripcion.innerHTML=automovil.descripcion

 const imagen = document.getElementById("imagen");
 imagen.src=automovil.imagen

 /*Arreglo de objetos*/
 automoviles=[{
    marca:"TOYOTA",
    modelo:"Supra",
    descripcion:"Toyota Supra 2024 Automatico Aire Acondicionado Precio $410,000",
    imagen:"/img/supra.png"
 },
 
 {
    marca:"NISSAN",
    modelo:"GT-R",
    descripcion:"Nissan GT-R 2020 Automatico Aire Acondicionado Precio $999,000",
    imagen:"/img/gtr.jpg"
 },
 
 {
    marca:"FORD",
    modelo:"Bronco",
    descripcion:"Ford Bronco 2024 Automatico Aire Acondicionado Precio $580,000",
    imagen:"/img/bronco.png"
 }];

 //mostrar todos los objetos
 for (let i = 0; i < automoviles.length; ++i) {
    console.log("marca: " + automoviles[i].marca);
    console.log("modelo: " + automoviles[i].modelo);
    console.log("descripcion: " + automoviles[i].descripcion);
    console.log("imagen: " + automoviles[i].imagen);
}

const selMarc = document.getElementById('selMarc');

selMarc.addEventListener('change', function() {

    let opcion = parseInt(selMarc.value);
    alert(opcion);

    const marcaElement = document.getElementById("marca");
    marcaElement.innerHTML = automoviles[opcion].marca;

    const modeloElement = document.getElementById("modelo");
    modeloElement.innerHTML = automoviles[opcion].modelo;

    const descripcionElement = document.getElementById("descripcion");
    descripcionElement.innerHTML = automoviles[opcion].descripcion;

    const imagenElement = document.getElementById("imagen");
    imagenElement.src = automoviles[opcion].imagen;
});
